const express = require("express");
const router = express.Router();
const userController = require("../controllers/userController");
const orderController = require("../controllers/orderController");
const cartController = require("../controllers/cartController");
const auth = require("../auth");

router.post("/checkEmail", (req,res) => {
	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));
});

router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(
		resultFromController));
});

router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(
		resultFromController));
});

router.post("/checkout", auth.verify, (req, res) => {
	let data = {
		userId: auth.decode(req.headers.authorization).id,
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		order: req.body
	};

	orderController.createOrder(data).then(resultFromController => res.send(
		resultFromController));
});

// router.get("/:userId/userDetails", auth.verify, (req, res) => {
// 	userController.getUserDetails(req.params).then(resultFromController => res.send(
// 		resultFromController));
// });

router.get("/details", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	console.log("This is a user");
	console.log(userData); 
	userController.getProfile({ userId: userData.id }).then(resultFromController => res.send(resultFromController));
});

router.get("/orders", auth.verify, (req, res) => {
	let	data = auth.decode(req.headers.authorization);
	orderController.getAllOrders({isAdmin: data.isAdmin}).then(resultFromController => res.send(
		resultFromController));
});

router.get("/myOrders", auth.verify, (req, res) => {
	let data = auth.decode(req.headers.authorization);
	orderController.getUserOrders(data).then(resultFromController => res.send(
		resultFromController));
});

router.put("/:userId/setAsAdmin", auth.verify, (req, res) => {
	let data = auth.decode(req.headers.authorization);
	userController.setUserAsAdmin(data, req.params).then(resultFromController => res.send(
		resultFromController));
});

router.post("/addToCart", auth.verify, (req, res) => {
	let data = auth.decode(req.headers.authorization);
	cartController.addToCart(data, req.body).then(resultFromController => res.send(
		resultFromController));
});

router.put("/changeQuantity", auth.verify, (req, res) => {
	let data = auth.decode(req.headers.authorization);
	cartController.changeProductQuantity(data, req.body).then(resultFromController => res.send(
		resultFromController));
});

router.put("/removeFromCart", auth.verify, (req, res) => {
	let data = auth.decode(req.headers.authorization);
	cartController.removeProductFromCart(data, req.body).then(resultFromController => res.send(
		resultFromController));
});

router.get("/getCart", auth.verify, (req, res) => {
	let data = auth.decode(req.headers.authorization);
	cartController.getUserCart(data).then(resultFromController => res.send(
		resultFromController));
})

module.exports = router;