const mongoose = require("mongoose");

const cartSchema = new mongoose.Schema({
	userId: {
		type: String,
		required: [true, "UserId is required."]
	},
	products: [
		{
			productId: {
				type: String,
				required: [true, "ProductId is required."]
			},
			quantity: {
				type: Number,
				required: [true, "Quantity is required."]
			},
			subTotal: {
				type: Number,
				required: [true, "subTotal is required."]
			}
		}
	],
	totalAmount: {
		type: Number,
		required: [true, "Total amount is required."]
	}
})

module.exports = mongoose.model("Cart", cartSchema);