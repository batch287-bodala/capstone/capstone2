const Order = require("../models/Order");
const Product = require("../models/Product");

module.exports.createOrder = async (data) => {
	if(!data.isAdmin){

		const products = data.order.products;
		let amount = 0;
		for(const p of products){
			const product = await Product.findById(p.productId);
			if(!product){
				return false;
			}
      		amount += (product.price)*(p.quantity);
		}
		let newOrder = new Order({
			userId: data.userId,
			products: data.order.products,
			totalAmount: amount
		});

		return newOrder.save().then((order, error) => {
			if(error){
				return false;
			} else {
				return true;
			}
		});
	}

	let message = Promise.resolve("Only non-admin users can place orders.")
	return message.then((value) => {
		return value
	});
}

module.exports.getAllOrders = (data) => {
	if(data.isAdmin){
		return Order.find({}).then(result => {
			return result;
		});
	} 
	let message = Promise.resolve("Only admin users can access this.")
	return message.then((value) => {
		return value
	});
}

module.exports.getUserOrders = (data) => {
	console.log(data);
	if(!data.isAdmin){
		return Order.find({ userId: data.id }).then(result => {
			return result;
		});
	}
	let message = Promise.resolve("Only non-admin users can access this.")
	return message.then((value) => {
		return value
	});
}
