const Cart = require("../models/Cart");
const Product = require("../models/Product");

module.exports.addToCart = async (data, reqBody) => {
	if(!data.isAdmin){
		cart = await Cart.findOne({ userId: data.id });
		if(!cart){  //Cart for this user does not exist. Create a cart
			newCart = new Cart({
				userId: data.id,
				totalAmount: 0
			})
		
			let amount = 0;
			const product = await Product.findById(reqBody.productId);
			if(!product){
				return false;
			}
			subtotal = (product.price)*(reqBody.quantity);
			newCart.products.push({
				productId: reqBody.productId,
				quantity: reqBody.quantity,
				subTotal: subtotal
			})
	      	amount += subtotal;
			newCart.totalAmount = amount;

			return newCart.save().then((cart, error) => {
				if(error){
					return false;
				} else {
					return true;
				}
			});
		} else {     //Cart already exists. Add to cart
			const existingProduct = cart.products.find((item) => item.productId === reqBody.productId);
			if(existingProduct){
				return "Product already exists in cart";
			}
			let amount = 0;
			const product = await Product.findById(reqBody.productId);
			if(!product){
				return false;
			}
			subtotal = (product.price)*(reqBody.quantity);
			cart.products.push({
				productId: reqBody.productId,
				quantity: reqBody.quantity,
				subTotal: subtotal
			})
	      	amount += subtotal;
			cart.totalAmount += amount;
			return cart.save().then((updatedCart, error) => {
    			if (error) {
      				return false;
    			} else {
      				return true;
    			}
  			});
		}

	}
	let message = Promise.resolve("Only non-admin users can use this feature.")
	return message.then((value) => {
		return value
	});
}


module.exports.changeProductQuantity = async (data, reqBody) => {
	if(!data.isAdmin){
		cart = await Cart.findOne({ userId: data.id });
		if(!cart){   //Cart does not exist
			return false;
		}
		const existingProduct = cart.products.find((item) => item.productId === reqBody.productId);
		if(!existingProduct){    //Product is not in cart
			return false;
		}
		const product = await Product.findById(reqBody.productId);
		oldSubTotal = existingProduct.subTotal;
		existingProduct.subTotal = product.price * reqBody.quantity;
		existingProduct.quantity = reqBody.quantity;
		cart.totalAmount = cart.totalAmount - oldSubTotal + existingProduct.subTotal;

		return cart.save().then((updatedCart, error) => {
			if(error){
				return false;
			} else {
				return true;
			}
		});
	}
	let message = Promise.resolve("Only non-admin users can use this feature.")
	return message.then((value) => {
		return value
	});
}


module.exports.removeProductFromCart = async (data, reqBody) => {
	if(!data.isAdmin){
		cart = await Cart.findOne({ userId: data.id });
		if(!cart){    //Cart does not exist
			return false;
		}
		const index = cart.products.findIndex((item) => item.productId === reqBody.productId);
		if(index === -1){    //Product is not in cart
			return false;
		}
		subtotal = cart.products[index].subTotal;
		cart.products.splice(index, 1);
		cart.totalAmount -= subtotal;

		return cart.save().then((updatedCart, error) => {
      		if (error) {
        		return false;
      		} else {
        		return true;
      		}
    	});
	}
	let message = Promise.resolve("Only non-admin users can use this feature.")
	return message.then((value) => {
		return value
	});
}

module.exports.getUserCart = (data) => {
	if(!data.isAdmin){
		return Cart.find({ userId: data.id }).then(result => {
		return result;
		});
	}
	let message = Promise.resolve("Only non-admin users can use this feature.")
	return message.then((value) => {
		return value
	});
}