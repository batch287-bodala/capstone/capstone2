Session s42:
	touch index.js .gitignore guide.js
	npm init -y
	npm install express mongoose cors
	create User model
	create Product model
	create Order model
	npm install bcrypt
	npm install jsonwebtoken
	create a route for registering user
	create a route for user login with authentication

Session s43:
	create a route for creating a product with authorization
	create a route for retrieving all products
	create a route for retrieving all active products

Session s44:
	create a route for retrieving a specific product
	create a route for updating a product with authorization
	create a route for archiving a product with authorization

Session s45:
	create a route for creating an order with authentication
	create a route for retrieving the details of a specific user with authentication
	create a route for retrieving all orders with authorization
	create a route for retrieving all orders of a user with authentication
	create a route for setting a user as admin

Stretch goals:
	create a model for cart 
	create a route for adding a product to cart 
	create a route for updating quantities of a product in cart
	create a route to retrieve cart details for a user
	create a route for removing a product from cart